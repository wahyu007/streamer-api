const express = require('express');

const router = express.Router();
const movies = require('../assets/MovieResponses.json');
const tvShow = require('../assets/TvshowResponses.json');

router.get('/movies', (req, res) => {
  res.status(200).send(movies);
});

router.get('/tvs', (req, res) => {
  res.status(200).send(tvShow);
});

module.exports = router;
